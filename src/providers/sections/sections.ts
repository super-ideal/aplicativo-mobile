import { Injectable } from '@angular/core';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/retry';

import { ApiService } from '../api';

@Injectable()
export class SectionsProvider {

  constructor(public api: ApiService) {
  }

  public getSectionsByType(idType: number, page: number = 1, wBy: string = 'id', w: string = '') {
    return new Promise((resolve, reject) => {
      this.api.get(`app/section/types/${idType}/?detailsSections=true&page=${page}&wBy=${wBy}&w=${w}`)
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
