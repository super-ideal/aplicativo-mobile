export class AppConfig {
  public apiEndpoint: string;

  constructor() {
    this.apiEndpoint = 'https://apiideal.herokuapp.com/v1/';
  }
}
