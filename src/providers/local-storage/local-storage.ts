import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class LocalStorageProvider {

  constructor(private storage: Storage) { }

  public set(key: string, value: any) {
    return this.storage.set(key, value);
  }

  public get(key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get(key).then(data => {
        resolve(data)
      }, err => {
        reject(err)
      });
    });
  }

  public remove(key: string) {
    return this.storage.remove(key);
  }

  public clear() {
    return this.storage.clear();
  }

  public keys() {
    return this.storage.keys();
  }
}
