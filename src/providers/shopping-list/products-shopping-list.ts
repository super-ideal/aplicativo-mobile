import { Injectable } from '@angular/core';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/retry';

import { ApiService } from '../api';

@Injectable()
export class ShoppingListProductsProvider {

  constructor(public api: ApiService) {
  }

  public getAllPublic(idList: number) {
    return new Promise((resolve, reject) => {
      this.api.get(`app/shopping/lists/${idList}/product/public`)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public getAll(idList: number) {
    return new Promise((resolve, reject) => {
      this.api.get(`app/shopping/lists/${idList}/product`)
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(product_id: number, list_id: number, quantity: number = null) {
    return new Promise((resolve, reject) => {
      this.api.post('app/shopping/lists/product', { product_id: product_id, list_id: list_id, quantity: quantity })
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public update(idList: number, idProductList: number, body: any) {
    return new Promise((resolve, reject) => {
      this.api.patch(`app/shopping/lists/${idList}/product/${idProductList}`, body)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public delete(idList: number, idProductList: number) {
    return new Promise((resolve, reject) => {
      this.api.delete(`app/shopping/lists/${idList}/product/${idProductList}`)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }
}
