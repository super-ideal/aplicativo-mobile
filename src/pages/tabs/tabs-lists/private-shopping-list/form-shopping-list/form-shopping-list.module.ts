import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormShoppingListPage } from './form-shopping-list';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    FormShoppingListPage,
  ],
  imports: [
    IonicPageModule.forChild(FormShoppingListPage),
    PipesModule
  ]
})
export class FormShoppingListPageModule {}
