import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';

import { FlashProvider } from '../../../../../../providers/shared/flash/flash';
import { ShoppingListProvider } from '../../../../../../providers/shopping-list/shopping-list';

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {
  loading: any;
  list: any;

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public flashProvider: FlashProvider,
    public toast: Toast,
    public _shoppingListProvider: ShoppingListProvider
  ) {
    this.list = this.navParams.get('list');
  }

  deleteList() {
    const confirm = this.alertCtrl.create({
      title: 'Excluir lista',
      message: 'Deseja realmente excluir a lista de compra selecionada?',
      buttons: [
        {
          text: 'Não'
        },
        {
          text: 'Sim',
          handler: () => {
            this.showLoader('Excluindo lista...');

            this._shoppingListProvider.delete(this.list['id'])
              .then((res) => {
                this.loading.dismiss();
                this.showToast('Lista de compra excluída com sucesso');
                this.viewCtrl.dismiss({ status: 'deleted' });
              }, (err) => {
                console.log('erro ', err);

                this.loading.dismiss();
                this.flashProvider.show('Erro ao excluir a lista de compra', 'error')
                this.viewCtrl.dismiss({ status: 'error' });
              });
          }
        }
      ]
    });
    confirm.present();
  }

  editList() {
    const modal = this.modalCtrl.create(
      'FormShoppingListPage',
      { dataList: this.list },
      { enableBackdropDismiss: true, showBackdrop: true }
    )
    modal.present();

    modal.onDidDismiss(data => {
      this.viewCtrl.dismiss({ status: 'updated' });
    });
  }

  showToast(message) {
    this.toast.show(message, '5000', 'bottom').subscribe(toast => { });
  }

  showLoader(message) {
    this.loading = this.loadingCtrl.create({
      content: message
    });

    this.loading.present();
  }
}
