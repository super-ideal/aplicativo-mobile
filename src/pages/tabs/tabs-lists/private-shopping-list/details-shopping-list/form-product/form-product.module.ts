import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonicImageLoader } from 'ionic-image-loader';
import { FormProductPage } from './form-product';

@NgModule({
  declarations: [
    FormProductPage,
  ],
  imports: [
    IonicPageModule.forChild(FormProductPage),
    IonicImageLoader
  ],
  exports: [
     FormProductPage
  ]
})

export class FormProductModule { }
