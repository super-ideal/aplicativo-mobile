import { Component } from '@angular/core';
import { ViewController, IonicPage, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FlashProvider } from '../../../../../../providers/shared/flash/flash';
import { Toast } from '@ionic-native/toast';
import { ShoppingListProductsProvider } from '../../../../../../providers/shopping-list/products-shopping-list';

@IonicPage()
@Component({
  selector: 'page-form-product',
  templateUrl: 'form-product.html'
})
export class FormProductPage {

  form: FormGroup;
  idList: any;
  productList: any;
  product: any;
  loading: any;

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public flashProvider: FlashProvider,
    public loadingCtrl: LoadingController,
    public toast: Toast,
    public _shoppingListProductsProvider: ShoppingListProductsProvider
  ) {
    this.idList = this.navParams.get('idList');
    this.productList = this.navParams.get('productList');
    this.product = this.productList.product;

    if (this.product && this.product.images) {
      let data = this.product.images;
      data.forEach((element, index) => {
        data[index]['url'] = `http://54.233.242.37/image/product/${element['filename']}?format=jpg&quality=80&width=100`
      });

      this.product.images = data;
    }

    this.form = new FormGroup({
      quantity: new FormControl(null, [Validators.required]),
      quantity_purchased: new FormControl(null)
    });
  }

  ionViewDidLoad() {
    this.form.patchValue({
      quantity: this.productList.quantity,
      quantity_purchased: this.productList.quantity_purchased
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  async submit() {
    this.showLoader();
    
    await this._shoppingListProductsProvider.update(this.idList, this.productList['id'], this.form.value)
      .then((res) => {
        this.showToast('Produto atualizado com sucesso');
        this.loading.dismiss();
        this.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.flashProvider.show('Erro interno. Tente novamente!', 'error', 3000)
        console.log('erro ', err);
      });
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  showToast(message) {
    this.toast.show(message, '5000', 'bottom').subscribe(toast => { });
  }
}
