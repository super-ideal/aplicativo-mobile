import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs-lists',
  templateUrl: 'tabs-lists.html'
})
export class TabsListsPage {

  privateRoot = 'PrivateShoppingListPage'
  publicRoot = 'PublicShoppingListPage'

  constructor(public navCtrl: NavController) {
    this.navCtrl.id
  }

}
