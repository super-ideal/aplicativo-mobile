import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicShoppingListPage } from './public-shopping-list';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { AccordionListModule } from '../../../../components/accordion-list/accordion-list.module';

@NgModule({
  declarations: [
    PublicShoppingListPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicShoppingListPage),
    AccordionListModule
  ],
  providers: [
    BarcodeScanner
  ]
})
export class PublicShoppingListModule {}
