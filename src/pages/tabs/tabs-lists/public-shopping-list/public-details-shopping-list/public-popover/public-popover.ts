import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { FlashProvider } from '../../../../../../providers/shared/flash/flash';
import { Toast } from '@ionic-native/toast';

import { ShoppingListProvider } from '../../../../../../providers/shopping-list/shopping-list';

@IonicPage()
@Component({
  selector: 'page-public-popover',
  templateUrl: 'public-popover.html',
})
export class PublicPopoverPage {
  loading: any;
  list: any;

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public flashProvider: FlashProvider,
    public toast: Toast,
    public _shoppingListProvider: ShoppingListProvider
  ) {
    this.list = this.navParams.get('list');
  }

  copyList() {
    const confirm = this.alertCtrl.create({
      title: 'Copiar lista',
      message: 'Deseja realmente copiar a lista de compra selecionada?',
      buttons: [
        {
          text: 'Não'
        },
        {
          text: 'Sim',
          handler: () => {
            this.showLoader('Copiando lista...');

            this._shoppingListProvider.copyList(this.list['id'])
              .then((res) => {
                this.loading.dismiss();
                this.showToast('Lista de compra copiada com sucesso', '8000');
                this.viewCtrl.dismiss({ status: 'copied', data: res });
              }, (err) => {
                console.log('erro ', err);
                
                this.loading.dismiss();
                this.flashProvider.show('Erro ao copiar a lista de compra', 'error')
                this.viewCtrl.dismiss({ status: 'error', data: err });
              });
          }
        }
      ]
    });
    confirm.present();
  }

  showLoader(message) {
    this.loading = this.loadingCtrl.create({
      content: message
    });

    this.loading.present();
  }

  showToast(message, time = '5000') {
    this.toast.show(message, time, 'bottom').subscribe(toast => { });
  }
}
