import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ProductsProvider } from '../../../../providers/products/products';
import { FlashProvider } from '../../../../providers/shared/flash/flash';
import { LocalStorageProvider } from '../../../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-list-products',
  templateUrl: 'list-products.html',
})
export class ListProductsPage {
  loading: any;

  category: any;
  displayType: boolean = true;

  searchTerm: string;
  searching: any = false;

  products: any[] = [];
  productsOriginal: any[] = [];
  page = 1;
  perPage = 0;
  total = 0;
  totalPage = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public flashProvider: FlashProvider,
    public localStorageProvider: LocalStorageProvider,
    public _productsProvider: ProductsProvider,
  ) {
    this.category = this.navParams.get('category');
  }

  async ionViewDidLoad() {
    this.showLoader();
    await this.getProducts(true);
  }

  async getProducts(clearProducts = false, page?: number, wBy?: string, w?: string) {
    // Id do tipo Categoria no BD é 1
    await this._productsProvider.getBySection(this.category.id, page, wBy, w)
      .then((res) => {
        this.total = res['data']['total'];
        this.perPage = res['data']['perPage'];
        this.page = res['data']['page'];

        if (clearProducts) {
          this.products = [];
          this.productsOriginal = [];
        }

        res['data']['data'].forEach(element => {
          this.products.push(element);
          this.productsOriginal.push(element);
        });

        this.localStorageProvider.set('products-category' + this.category.id, this.products);
        this.loading.dismiss();
      }, (err) => {
        console.log('erro ', err);

        this.loading.dismiss();
        if (err.status !== 404) {
          this.flashProvider.show('Ocorreu um erro ao buscar os produtos', 'error', 3000)
        }

        this.localStorageProvider.get('products-category' + this.category.id).then(res => {
          this.products = res ? res : [];
          this.productsOriginal = res ? res : [];
        });
      });
  }

  async filterItems() {
    this.searching = true;

    this.products = await this.productsOriginal.filter((item) => {
      return item.description.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
    });

    this.searching = false;
  }

  doInfinite(infiniteScroll) {
    this.page += 1;

    setTimeout(() => {
      this.getProducts(false, this.page);
      infiniteScroll.complete();
    }, 2000);
  }

  async doRefresh(refresher) {
    setTimeout(async () => {
      await this.getProducts(true);
      refresher.complete();
    }, 2000);
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  protected changeDisplayType(type: string) {
    if (type == 'grid')
      this.displayType = true;
    else
      this.displayType = false;
  }
}
