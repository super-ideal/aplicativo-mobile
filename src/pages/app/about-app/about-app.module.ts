import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutAppPage } from './about-app';
import { AppVersion } from '@ionic-native/app-version';

@NgModule({
  declarations: [
    AboutAppPage,
  ],
  imports: [
    IonicPageModule.forChild(AboutAppPage),
  ],
  providers: [
    AppVersion,
  ]
})
export class AboutAppPageModule {}
