import { Component } from '@angular/core';
import { ViewController, IonicPage, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { FlashProvider } from '../../providers/shared/flash/flash';
import { ShoppingListProductsProvider } from '../../providers/shopping-list/products-shopping-list';
import { ShoppingListProvider } from '../../providers/shopping-list/shopping-list';

@IonicPage()
@Component({
  selector: 'page-modal-add-product-list',
  templateUrl: 'modal-add-product-list.html'
})
export class ModalAddProductListPage {

  idList: any;
  product: any;
  loading: any;

  lists: any[] = [];
  page = 1;
  perPage = 0;
  total = 0;
  totalPage = 0;

  constructor(
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public flashProvider: FlashProvider,
    public loadingCtrl: LoadingController,
    public toast: Toast,
    public _shoppingListProductsProvider: ShoppingListProductsProvider,
    public _shoppingListProvider: ShoppingListProvider
  ) {
    this.product = this.navParams.get('product');

    if (this.product && this.product.images) {
      let data = this.product.images;
      data.forEach((element, index) => {
        data[index]['url'] = `http://54.233.242.37/image/product/${element['filename']}?format=jpg&quality=80&width=100`
      });

      this.product.images = data;
    }
    console.log(this.product)
  }

  ionViewDidEnter() {
    this.getLists(true);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addToList(list) {
    this.idList = list.id;
    this.alertConfirmPostProduct();
  }

  alertConfirmPostProduct() {
    const prompt = this.alertCtrl.create({
      title: 'Adicionar produto a lista',
      message: "Informe a quantidade a comprar do produto selecionado",
      inputs: [
        {
          name: 'quantity',
          type: 'number',
          value: '1'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            this.idList = null;
          }
        },
        {
          text: 'Adicionar',
          handler: data => {
            if (data) {
              this.postProduct(data.quantity);
            }
          }
        }
      ]
    });
    prompt.present();
  }

  async postProduct(quantity) {
    this.showLoader();
    
    await this._shoppingListProductsProvider.post(this.product.id, this.idList, quantity)
      .then((res) => {
        this.showToast('Produto adicionado a lista de compras');
        this.loading.dismiss();
        this.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.flashProvider.show('Erro interno. Tente novamente!', 'error', 3000)
        console.log('erro ', err);
      });
  }

  async getLists(clearList = false, page?: number, wBy?: string, w?: string) {
    this.showLoader();

    // Id do tipo Categoria no BD é 1
    await this._shoppingListProvider.getAll(true, true, page, wBy, w)
      .then((res) => {
        this.loading.dismiss();

        this.total = res['data']['total'];
        this.perPage = res['data']['perPage'];
        this.page = res['data']['page'];

        if (clearList) {
          this.lists = [];
        }

        res['data']['data'].forEach(element => {
          this.lists.push(element)
        });
      }, (err) => {
        this.loading.dismiss();
        this.flashProvider.show('Erro interno. Tente novamente!', 'error', 3000)
        console.log('erro ', err);
      });
  }

  doInfinite(infiniteScroll) {
    this.page += 1;

    setTimeout(() => {
      this.getLists(false, this.page);
      infiniteScroll.complete();
    }, 2000);
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  showToast(message) {
    this.toast.show(message, '5000', 'bottom').subscribe(toast => { });
  }
}
