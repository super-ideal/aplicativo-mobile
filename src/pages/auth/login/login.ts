import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { FlashProvider } from '../../../providers/shared/flash/flash';
import { AuthProvider } from '../../../providers/auth/auth';
import { LocalStorageProvider } from '../../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  form: FormGroup;
  params: any;
  name: any;
  errorPassword: Boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public flashProvider: FlashProvider,
    private localStorage: LocalStorageProvider,
    public _auth: AuthProvider,
    public events: Events
  ) {
    this.form = new FormGroup({
      password: new FormControl(null, [Validators.required])
    });
  }

  ionViewDidLoad() {
    this.name = this.navParams.get('name');
    this.params = this.navParams.get('params');
  }

  protected submit(): void {
    if (this.form.invalid) {
      return this.flashProvider.show('Formulário inválido. Verifique os dados e tente novamente!', 'error');
    }

    let loader = this.loadingCtrl.create({});
    loader.present();

    let body = this.params;
    body['password'] = this.form.get('password').value;

    this._auth.login(body).then(async res => {
      const dataLogin = res;

      // Salva token LocalStorage
      this.localStorage.set('token', dataLogin['data']['auth']['token']);

      // Salva refreshToken LocalStorage
      this.localStorage.set('refreshToken', dataLogin['data']['auth']['refreshToken']);

      // Salva dados do usuário
      this.localStorage.set('user', res['data']['user']);
      this.events.publish('user:logged', res['data']['user']); // Atualiza Sidemenu
      this.navCtrl.setRoot('TabsPage', { params: res['data']['user'] });

      loader.dismiss();
    }, (err) => {
      loader.dismiss();

      if (err.status == 401) {
        this.errorPassword = true;
      } else {
        this.flashProvider.show(err.status + ' - Não conseguimos processar sua solicitação!', 'error', 10000);
      }
    });
  }

  protected openPage(page): void {
    page.setRoot == true ? this.navCtrl.setRoot(page.component) : this.navCtrl.push(page.component);
  }

  protected goBack(): void {
    this.navCtrl.pop();
  }
}
