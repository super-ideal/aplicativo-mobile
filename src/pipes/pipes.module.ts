import { NgModule } from '@angular/core';
import { PickUpCertainWordsPipe } from './pick-up-certain-words/pick-up-certain-words';

@NgModule({
	declarations: [
    PickUpCertainWordsPipe,
  ],
	imports: [],
	exports: [
    PickUpCertainWordsPipe
  ]
})
export class PipesModule {}
