import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Config, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Toast } from '@ionic-native/toast';

import { ImageLoaderConfig } from 'ionic-image-loader';

import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navChild: Nav;

  protected rootPage: string = 'TabsPage';
  private activePage: any;
  protected pages: Array<{ title: string, icon: string, badge?: string, component: string, setRoot?: boolean }>;

  protected chosenPicture: string = 'assets/imgs/menu/avatar-login.png';
  protected userLogged: any = null;
  protected online: boolean = false;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private config: Config,
    public events: Events,
    private toast: Toast,
    private localStorage: LocalStorageProvider,
    private connectivityProvider: ConnectivityProvider,
    private imageLoaderConfig: ImageLoaderConfig
  ) {
    this.initializeApp();

    // Seta configuração do image loader
    this.imageLoaderConfig.enableSpinner(true);
    this.imageLoaderConfig.setConcurrency(15);
    this.imageLoaderConfig.setFallbackUrl('assets/imgs/no-img.png');
    this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
    this.imageLoaderConfig.useImageTag(true);

    // Eventos para atualizar o sidemenu
    events.subscribe('user:logged', (user) => {
      this.userLogged = user;
      this.toast.show(`Seja bem vindo!`, '5000', 'bottom').subscribe(toast => { });
    });

    events.subscribe('user:logout', () => {
      this.userLogged = null;
      this.toast.show(`Você saiu com sucesso!`, '5000', 'bottom').subscribe(toast => {});
    });

    // Define configuração no IOS
    this.config.set('ios', 'backButtonText', 'Voltar');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.verifyNetwork();
      this.initMenu();
      this.getUser();
      this.hideSplashScreen();
    });
  }

  private getUser(): void {
    this.localStorage.get('user')
    .then(res => {
      this.events.publish('user:logged', res); // Atualiza Sidemenu
    });
  }

  protected logout(): void {
    this.localStorage.clear();
    this.events.publish('user:logout'); // Atualiza Sidemenu
  }

  private verifyNetwork() {
    if (this.connectivityProvider.isOnline())
      this.online = true;

    this.connectivityProvider.watchOffline().subscribe(() => {
      this.online = false;
    });

    this.connectivityProvider.watchOnline().subscribe(() => {
      this.online = true;
    });
  }

  private hideSplashScreen(): void {
    setTimeout(() => {
      this.splashScreen.hide();
    }, 100);
  }

  private initMenu(): void {
    this.pages = [
      { title: 'Home', icon: 'home', component: 'HomePage', setRoot: true },
      { title: 'Contato', icon: 'contact', component: 'ContactPage', setRoot: false },
    ];
  }

  protected openPage(page): void {
    page.setRoot == true ? this.navChild.setRoot(page.component) : this.navChild.push(page.component);
    this.activePage = page;
  }

  protected checkActive(page) {
    return page == this.activePage;
  }
}
