import { Component, ViewChild } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';

import { TimeBarComponent } from './time-bar/time-bar';
import { FlashProvider } from '../../../providers/shared/flash/flash';

// https://www.joshmorony.com/creating-a-flash-message-service-in-ionic/

@Component({
  selector: 'flash',
  templateUrl: 'flash.html',
  animations: [
    trigger('messageState', [
      transition('void => *', [
        style({ transform: 'translateY(+100%)' }),
        animate('200ms ease-out')
      ]),
      transition('* => void', [
        animate('200ms ease-in', style({ opacity: '0' }))
      ])
    ])
  ]
})
export class FlashComponent {
  @ViewChild(TimeBarComponent) set tb(timeBar: TimeBarComponent) {
    if (typeof (timeBar) !== 'undefined') {
      timeBar.startTimer(this.duration);
    }
  }

  protected active: boolean = false;
  private timeout: any;

  protected message: string;
  protected dismiss: string = '(clique para fechar)';
  protected duration: number;
  protected activeClass: string;
  protected position: string = 'bottom';

  constructor(private flashProvider: FlashProvider) {
    this.flashProvider.show = this.show.bind(this);
    this.flashProvider.hide = this.hide.bind(this);
  }

  show(message: string, type?: string, duration?: number) {
    this.active = true;
    this.message = message;
    this.duration = duration;
    this.activeClass = type ? type : 'secondary';

    if (duration) {
      this.timeout = setTimeout(() => {
        this.active = false;
      }, duration);
    }
  }

  hide() {
    this.active = false;
    clearTimeout(this.timeout);
  }
}
